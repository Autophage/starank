# STARank

**STARank** is an ELO ranking system, developed in Qt primarily with small 
chess clubs in mind. Currently only a Windows version is available, which may 
be found [here](http://www.mediafire.com/file/vpcfxwwmud5mbxn/STARank.zip/file), 
but if the desire is there, I will likely provide one for Linux as well.

## Players
![Managing Players](/sample_images/players.PNG?raw=true "Managing Players")

## Game Recording
![Recording Games](/sample_images/games.png?raw=true "Recording Games")

## Rankings
![Generating a Ranking Table](/sample_images/rankings.PNG?raw=true "Generating a Ranking Table")
![Ranking Table](/sample_images/rankings_table.PNG?raw=true "Ranking Table")