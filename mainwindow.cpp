#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "playerviewdelegate.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // SQL SETUP //

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("starank.db");

    if (!db.open()) qDebug() << "Cannot open database";

    const QString CREATE_PLAYERS = "CREATE Table players ( "
                              "id INTEGER PRIMARY KEY, "
                              "name VARCHAR(255) NOT NULL UNIQUE, "
                              "score UNSIGNED INT NOT NULL DEFAULT 1500, "
                              "archived BOOLEAN NOT NULL DEFAULT FALSE "
                              ")";

    const QString CREATE_GAMES = "CREATE Table games ( "
                            "id INTEGER PRIMARY KEY, "
                            "player1Id UNSIGNED INT NOT NULL, "
                            "player2Id UNSIGNED INT NOT NULL, "
                            "winningPlayerId UNSIGNED INT NOT NULL DEFAULT 0, "
                            "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP "
                            ")";

    // Create tables where needed
    query = QSqlQuery();
    if (!db.tables().contains("games")) query.exec(CREATE_GAMES);
    if (!db.tables().contains("players")) query.exec(CREATE_PLAYERS);
}

MainWindow::~MainWindow()
{
    db.close();
    delete gamesListModel;
    delete playersDropdownModel;
    delete playersListModel;
    delete ui;
}


// SIDEBAR FUNCTIONS //

void MainWindow::on_btnLogo_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

    ui->btnGames->setChecked(false);
    ui->btnPlayers->setChecked(false);
    ui->btnRank->setChecked(false);
}

void MainWindow::on_btnGames_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);

    ui->btnGames->setChecked(true);
    ui->btnPlayers->setChecked(false);
    ui->btnRank->setChecked(false);

    populateDropdowns();
}

void MainWindow::on_btnPlayers_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);

    ui->btnGames->setChecked(false);
    ui->btnPlayers->setChecked(true);
    ui->btnRank->setChecked(false);

    setupPlayers();
}

void MainWindow::on_btnRank_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);

    ui->btnGames->setChecked(false);
    ui->btnPlayers->setChecked(false);
    ui->btnRank->setChecked(true);

    setupRanks();
}
