#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QInputDialog>
#include <QItemSelectionModel>
#include <QMainWindow>
#include <QResizeEvent>
#include <QSqlDatabase>
#include <QtSql>

#define GAME_CREATE_PREPARATION "INSERT INTO games (player1Id, player2Id, winningPlayerId) VALUES (:player1Id, :player2Id, :winningPlayerId)"

#define PLAYER_CREATE_PREPARATION "INSERT INTO players (name) VALUES (:name)"
#define PLAYER_ARCHIVE_PREPARATION "UPDATE players set archived = (:archived) WHERE id = (:id)"
#define PLAYER_RENAME_PREPARATION "UPDATE players set name = (:name) WHERE id = (:id)"
#define PLAYER_SCORE_PREPARATION "UPDATE players SET score = (:score) WHERE id = (:id)"
#define PLAYER_DELETE_PREPARATION "DELETE FROM players WHERE id = (:id)"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    // VARIABLES //
    Ui::MainWindow *ui;

    QSqlDatabase db = QSqlDatabase();
    QSqlQuery query;

    // Players
    QSqlQueryModel *playersListModel = new QSqlQueryModel();
    QSqlQueryModel *playersDropdownModel = new QSqlQueryModel();
    int archived = 0;
    int index = -1;
    QString sort = "name";

    // Games
    struct gameRecord {
        int id;

        int id0;
        int score0;

        int id1;
        int score1;
    };

    QSqlQueryModel *gamesListModel = new QSqlQueryModel();
    QStringListModel *historyModel = new QStringListModel();
    QStringList history;
    QList<gameRecord> detailedHistory;
    int playerIndices[2] = {-1, -1};
    int winner = -2;
    int r0 = -1;
    int r1 = -1;

    // Ranks
    QString filename = "Outputs/rankings.html";
    QFile file;
    QDir dir;
    const QString RANKING_TABLE_STYLE =
            "<style>"
            ".t {"
            "text-align: center;"
            "font-size: 32px;"
            "padding: 2rem;"
            "width: 600px;"
            "}"
            "table {"
            "background: white;"
            "font-size: 24px;"
            "text-align: center;"
            "}"
            ".black {"
            "background: black;"
            "color: white;"
            "}"
            "</style>";


    // FUNCTIONS //
    // Players
    void updatePlayersList();
    void moveToNextPlayer();
    void clearPlayerInfo();

    // Games
    void setupPlayers();
    void populateDropdowns();
    void playerSelect(int index, int p);
    void updateScores();

    // Ranks
    void setupRanks();
    void displayRawHTML();


private slots:
    void on_btnLogo_clicked();
    void on_btnGames_clicked();
    void on_btnPlayers_clicked();
    void on_btnRank_clicked();

    // Players
    void on_playersTxtPlayer_returnPressed();
    void on_playersBtnAdd_clicked();
    void on_playersDropdownDisplay_currentIndexChanged(int index);
    void on_playersDropdownSort_currentIndexChanged(const QString &arg1);
    void on_playersListSelectionChanged(const QItemSelection &item);
    void on_playersBtnEdit_clicked();
    void on_playersBtnArchive_clicked();
    void on_playersBtnDelete_clicked();

    // Games
    void on_gamesBtnSubmit_clicked();
    void on_gamesBtnUndo_clicked();
    void on_gamesDropdownP0_currentIndexChanged(int index);
    void on_gamesDropdownP1_currentIndexChanged(int index);
    void on_gamesRadioP0_clicked();
    void on_gamesRadioP1_clicked();
    void on_gamesRadioDraw_clicked();

    // Ranks
    void on_ranksBtnExportHTML_clicked();
    void on_ranksBtnWebBrowserHTML_clicked();
};

#endif // MAINWINDOW_H
