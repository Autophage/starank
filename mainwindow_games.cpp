#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "rankingscalculator.h"

// GAME RECORDING FUNCTIONS //

void MainWindow::on_gamesBtnSubmit_clicked()
{
    int pi0 = playerIndices[0];
    int pi1 = playerIndices[1];
    if (pi0 == pi1) return;

    int id0 = gamesListModel->record(pi0).field(2).value().toInt();
    int id1 = gamesListModel->record(pi1).field(2).value().toInt();

    gameRecord g;
    g.id0 = id0;
    g.score0 = r0;
    g.id1 = id1;
    g.score1 = r1;

    // Update player rankings //
    int playerRankings[2] = {r0, r1};
    if (!RankingsCalculator::calculateRankings(playerRankings, winner)) return;

    query.prepare(PLAYER_SCORE_PREPARATION);
    query.bindValue(":score", playerRankings[0]);
    query.bindValue(":id", id0);
    if (!query.exec()) qDebug() << query.lastError();

    query.bindValue(":score", playerRankings[1]);
    query.bindValue(":id", id1);
    if (!query.exec()) qDebug() << query.lastError();

    gamesListModel->setQuery("SELECT name, score, id FROM players WHERE id != 0 AND archived = 0 ORDER BY name COLLATE NOCASE");
    ui->gamesDropdownP0->setCurrentIndex(pi0);
    ui->gamesDropdownP1->setCurrentIndex(pi1);

    r0 = gamesListModel->record(pi0).field(1).value().toInt();
    r1 = gamesListModel->record(pi1).field(1).value().toInt();

    // Save record of game //
    query.prepare(GAME_CREATE_PREPARATION);
    query.bindValue(":player1Id", (id0 < id1) ? id0 : id1);
    query.bindValue(":player2Id", (id0 < id1) ? id1 : id0);

    QString deltaScore = QString::number(std::abs(playerRankings[0] - g.score0));
    switch (winner) {
    case 0:
        query.bindValue(":winningPlayerId", id0);
        history << ui->gamesDropdownP0->currentText() + " defeated " + ui->gamesDropdownP1->currentText() + " (" + deltaScore + " points transferred)";
        break;
    case 1:
        query.bindValue(":winningPlayerId", id1);
        history << ui->gamesDropdownP1->currentText() + " defeated " + ui->gamesDropdownP0->currentText() + " (" + deltaScore + " points transferred)";
        break;
    case -1:
        query.bindValue(":winningPlayerId", 0);
        history << ui->gamesDropdownP0->currentText() + " tied with " + ui->gamesDropdownP1->currentText() + " (" + deltaScore + " points transferred)";
        break;
    default:
        return;
    }

    if (!query.exec()) qDebug() << query.lastError();
    if (!query.exec("SELECT MAX(id) FROM games")) qDebug() << query.lastError();
    query.next();
    g.id = query.value(0).toInt();

    detailedHistory << g;

    historyModel->setStringList(history);
    // TODO: Move
    ui->gamesListHistory->setSelectionMode(QAbstractItemView::NoSelection);
    ui->gamesListHistory->setModel(historyModel);
    ui->gamesListHistory->selectionModel()->select(historyModel->index(history.length() - 1), QItemSelectionModel::Select);
}

void MainWindow::on_gamesBtnUndo_clicked()
{
    if (detailedHistory.isEmpty()) return;

    gameRecord last = detailedHistory.takeLast();

    query.prepare(PLAYER_SCORE_PREPARATION);
    query.bindValue(":score", last.score0);
    query.bindValue(":id", last.id0);
    if (!query.exec()) qDebug() << query.lastError();

    query.bindValue(":score", last.score1);
    query.bindValue(":id", last.id1);
    if (!query.exec()) qDebug() << query.lastError();

    // TODO: Add this to the preprocessor stuff?
    if (!query.exec("DELETE FROM games WHERE id = " + QString::number(last.id))) qDebug() << query.lastError();

    history.takeLast();
    historyModel->setStringList(history);
    ui->gamesListHistory->setModel(historyModel);
    ui->gamesListHistory->selectionModel()->select(historyModel->index(history.length() - 1), QItemSelectionModel::Select);

    // TODO: Make this a function?
    int p0 = playerIndices[0];
    int p1 = playerIndices[1];
    gamesListModel->setQuery("SELECT name, score, id FROM players WHERE id != 0 AND archived = 0 ORDER BY name COLLATE NOCASE");
    ui->gamesDropdownP0->setCurrentIndex(p0);
    ui->gamesDropdownP1->setCurrentIndex(p1);
}

void MainWindow::on_gamesDropdownP0_currentIndexChanged(int index)
{
    playerSelect(index, 0);
}

void MainWindow::on_gamesDropdownP1_currentIndexChanged(int index)
{
    playerSelect(index, 1);
}

void MainWindow::on_gamesRadioP0_clicked()
{
    winner = 0;
    updateScores();
}

void MainWindow::on_gamesRadioP1_clicked()
{
    winner = 1;
    updateScores();
}

void MainWindow::on_gamesRadioDraw_clicked()
{
    winner = -1;
    updateScores();
}

void MainWindow::populateDropdowns()
{
    gamesListModel->setQuery("SELECT name, score, id FROM players WHERE id != 0 AND archived = 0 ORDER BY name COLLATE NOCASE");
    ui->gamesDropdownP0->setModel(gamesListModel);
    ui->gamesDropdownP0->setCurrentIndex(-1);
    ui->gamesDropdownP1->setModel(gamesListModel);
    ui->gamesDropdownP1->setCurrentIndex(-1);
}

void MainWindow::playerSelect(int index, int p) {
    int r = gamesListModel->record(playerIndices[p]).field(1).value().toInt();
    if (p == 0) r0 = r;
    else r1 = r;

    playerIndices[p] = index;
    updateScores();
}

void MainWindow::updateScores() {
    r0 = gamesListModel->record(playerIndices[0]).field(1).value().toInt();
    r1 = gamesListModel->record(playerIndices[1]).field(1).value().toInt();

    for (int i = 0; i < 2; i++){
        int playerRankings[2] = {r0, r1};
        int r = playerRankings[i];
        QString score = "Score: ";

        if (playerIndices[i] == -1) {
            score += "? -> ?";
            ui->gamesBtnSubmit->setDisabled(true);
        } else {
            score += QString::number(r) + " -> ";
            if (playerIndices[(i + 1) % 2] == -1 || std::abs(winner) > 1 || playerIndices[0] == playerIndices[1]) {
                score += "?";
                ui->gamesBtnSubmit->setDisabled(true);
            } else {
                RankingsCalculator::calculateRankings(playerRankings, winner);

                if (QString::number(playerRankings[i]) < QString::number(r)) {
                    score += "<span style=\"color:red;\">" + QString::number(playerRankings[i]) + "</span>";
                } else if (QString::number(playerRankings[i]) > QString::number(r)) {
                    score += "<span style=\"color:green;\">" + QString::number(playerRankings[i]) + "</span>";
                } else {
                    score += QString::number(playerRankings[i]);
                }

                ui->gamesBtnSubmit->setDisabled(false);
            }
        }

        if (i == 0) ui->gamesLblScore0->setText(score);
        else ui->gamesLblScore1->setText(score);
    }
}
