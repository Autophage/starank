#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "playerviewdelegate.h"

// PLAYER ENTRY FUNCTIONS //

void MainWindow::setupPlayers() {
    updatePlayersList();
    playersListModel->setHeaderData(0, Qt::Horizontal, tr("Name"));
    playersListModel->setHeaderData(1, Qt::Horizontal, tr("Score"));
    playersListModel->setHeaderData(2, Qt::Horizontal, tr("ID"));

    ui->playersList->setModel(playersListModel);
    ui->playersList->setItemDelegate(new PlayerViewDelegate(ui->playersList));

    clearPlayerInfo();

    connect(ui->playersList->selectionModel(),
      SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
      this, SLOT(on_playersListSelectionChanged(QItemSelection)));
}

void MainWindow::on_playersTxtPlayer_returnPressed()
{
    on_playersBtnAdd_clicked();
}

void MainWindow::on_playersBtnAdd_clicked()
{
    QString name = ui->playersTxtPlayer->text();
    if (!name.length()) return;

    query.prepare(PLAYER_CREATE_PREPARATION);
    query.bindValue(":name", name);

    if(!query.exec()) qDebug() << query.lastError().text();
    else updatePlayersList();

    ui->playersTxtPlayer->clear();
}

void MainWindow::on_playersDropdownDisplay_currentIndexChanged(int index)
{
    archived = index;
    archived
                    ? ui->playersBtnArchive->setText("Unarchive Player")
                    : ui->playersBtnArchive->setText("Archive Player");
    updatePlayersList();
    clearPlayerInfo();
}

void MainWindow::on_playersDropdownSort_currentIndexChanged(const QString &arg1)
{
    sort = arg1.toLower();
    updatePlayersList();
}

void MainWindow::updatePlayersList()
{
    const QString Q = "SELECT name, score, id FROM players WHERE id != 0 AND archived = " + QString::number(archived) + " ORDER BY " + sort + " COLLATE NOCASE " + (sort == "name" ? "ASC" : "DESC");
    playersListModel->setQuery(Q);
}

void MainWindow::on_playersListSelectionChanged(const QItemSelection &item)
{
    auto index = item.indexes()[0];

    this->index = index.row();
    QString name = playersListModel->record(index.row()).value(0).toString();
    QString score = playersListModel->record(index.row()).value(1).toString();
    QString id = playersListModel->record(index.row()).value(2).toString();

    ui->playersLblNameValue->setText(name);
    ui->playersLblScoreValue->setText(score);

    ui->playersBtnEdit->setEnabled(true);
    ui->playersBtnArchive->setEnabled(true);

    int wins = 0;
    int losses = 0;
    int draws = 0;

    query.exec("SELECT player1Id, player2Id, winningPlayerId FROM games WHERE player1Id = " + id + " OR player2Id = " + id);
    while (query.next()) {
        if (!query.value(2).toInt()) draws++;
        else if (query.value(2).toString() == id) wins++;
        else losses++;
    }

    ui->playersLblWinsValue->setText(QString::number(wins));
    ui->playersLblLossesValue->setText(QString::number(losses));
    ui->playersLblDrawsValue->setText(QString::number(draws));

    if (wins + losses + draws == 0) {
        ui->playersWinrate->setValue(0);
        ui->playersBtnDelete->setDisabled(false);
    } else {
        ui->playersWinrate->setValue(wins * 100 / (wins + losses + draws));
        ui->playersBtnDelete->setDisabled(true);
    }

    // TODO: Implement deletion (Delete players with games - delete their games and reassess all scores
}


void MainWindow::on_playersBtnEdit_clicked()
{
    bool ok;
    QString name = playersListModel->record(index).value(0).toString();
    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                         tr("User name:"), QLineEdit::Normal,
                                         name, &ok);

    if (ok && !text.isEmpty()) {
        query.prepare(PLAYER_RENAME_PREPARATION);

        query.bindValue(":id", playersListModel->record(index).value(2).toString());
        query.bindValue(":name", text);

        if(!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            updatePlayersList();
            ui->playersLblNameValue->setText(text);
        }
    }
}

void MainWindow::on_playersBtnArchive_clicked()
{
    query.prepare(PLAYER_ARCHIVE_PREPARATION);

    query.bindValue(":id", playersListModel->record(index).value(2).toString());
    query.bindValue(":archived", (archived + 1) % 2);
    if(!query.exec()) qDebug() << query.lastError().text();

    moveToNextPlayer();
}

void MainWindow::on_playersBtnDelete_clicked()
{
    query.prepare(PLAYER_DELETE_PREPARATION);

    query.bindValue(":id", playersListModel->record(index).value(2).toString());
    if(!query.exec()) qDebug() << query.lastError().text();

    moveToNextPlayer();
}

// Change the selection to the next player in the list when a player is archived or deleted
void MainWindow::moveToNextPlayer()
{
    updatePlayersList();

    QModelIndex i = playersListModel->index(index, 0);
    // Moves the selection up if (un)archiving the last player in the list
    while (i.row() == -1 && index >= 0) i = playersListModel->index(--index, 0);

    if (index < 0) clearPlayerInfo();

    ui->playersList->selectionModel()->select(i, QItemSelectionModel::Select);
}

void MainWindow::clearPlayerInfo()
{
    ui->playersLblNameValue->setText("-");
    ui->playersLblScoreValue->setText("-");
    ui->playersLblWinsValue->setText("-");
    ui->playersLblLossesValue->setText("-");
    ui->playersLblDrawsValue->setText("-");
    ui->playersWinrate->setValue(0);

    ui->playersBtnEdit->setEnabled(false);
    ui->playersBtnArchive->setEnabled(false);
    ui->playersBtnDelete->setEnabled(false);
}
