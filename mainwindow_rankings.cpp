#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QDesktopServices>
#include <QTextStream>
#include <QGraphicsOpacityEffect>

// PLAYER ENTRY FUNCTIONS //

void MainWindow::setupRanks() {
    QGraphicsOpacityEffect *eff = new QGraphicsOpacityEffect(this);
    eff->setOpacity(0);
    ui->ranksLblNotification->setGraphicsEffect(eff);

    if (!QDir("Outputs").exists()) {
        QDir().mkdir("Outputs");
    }

    file.setFileName(filename);
    if (!file.exists()) {
        ui->ranksBtnWebBrowserHTML->setDisabled(true);
    } else {
        displayRawHTML();
    }
}

void MainWindow::on_ranksBtnExportHTML_clicked()
{
    QString html =
                    "<table style='border: 1px solid black; border-collapse: collapse;'>"
                    "<tr>"
                    "<th style='width: 100px;'>RANK</th>"
                    "<th class='black' style='width: 500px;'>PLAYER</th>"
                    "<th style='width: 100px;'>SCORE</th>"
                    "</tr>";

    if (!query.exec("SELECT name, score FROM players WHERE id != 0 AND archived = 0 ORDER BY score COLLATE NOCASE DESC")) qDebug() << query.lastError();

    int i = 1;
    while (query.next()) {
        html += "<tr><td";
        if (i % 2) html += " class='black'";
        html += ">";

        // Rank
        switch (i) {
        case 1:
            html += "&#9812 1";
            break;
        case 2:
            html += "&#9813 2";
            break;
        case 3:
            html += "&#9814 3";
            break;
        case 4:
            html += "&#9815 4";
            break;
        case 5:
            html += "&#9816 5";
            break;
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
            html += "&#9817 " + QString::number(i);
            break;
        default:
            html += QString::number(i);
            break;
        }
        html += "</td>";

        // Name
        html += "<td";
        if ((i + 1) % 2) html += " class='black'";
        html += ">" + query.value(0).toString() + "</td>";

        // Score
        html += "<td";
        if (i % 2) html += " class='black'";
        html += ">" + query.value(1).toString() + "</td></tr>";

        i++;
    }
        html += "</table>";
        html += RANKING_TABLE_STYLE;

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream <<  html;
        file.close();

        ui->ranksLblNotification->setText("Exported HTML to \"" + filename + "\"");

        displayRawHTML();
        ui->ranksBtnWebBrowserHTML->setDisabled(false);
    } else {
        ui->ranksLblNotification->setText("Error occurred when writing to HTML file");
    }

    QGraphicsOpacityEffect *eff = new QGraphicsOpacityEffect(this);
    ui->ranksLblNotification->setGraphicsEffect(eff);
    QPropertyAnimation *a = new QPropertyAnimation(eff,"opacity");
    a->setDuration(5000);
    a->setStartValue(1);
    a->setEndValue(0);
    a->setEasingCurve(QEasingCurve::OutBack);
    a->start(QPropertyAnimation::DeleteWhenStopped);
}

void MainWindow::on_ranksBtnWebBrowserHTML_clicked()
{
    QDesktopServices::openUrl(QUrl("file:/" + dir.absolutePath() + "/" + filename));
}

void MainWindow::displayRawHTML()
{
    if (file.open(QIODevice::ReadWrite)) {
        QTextStream in(&file);
        QString code;

        while(!in.atEnd()) {
            QString line = in.readLine();
            code.append(line);
        }
        ui->ranksBrowserHTML->setPlainText(code);
        file.close();
    }
}
