#include "playerviewdelegate.h"
#include <QListView>

PlayerViewDelegate::PlayerViewDelegate(QObject *parent) :
    QItemDelegate(parent)
{
    model = qobject_cast<QListView*>(parent)->model();
}

void PlayerViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->setPen(Qt::black);
    if (option.state & QStyle::State_Selected) {
        // painter->fillRect(option.rect, option.palette.highlight());
        painter->fillRect(option.rect, "#880011");
        painter->setPen(Qt::white);
    } else if (option.state & QStyle::State_MouseOver) {
        painter->fillRect(option.rect, QColor(152, 0, 11, 55));

    }

    painter->setFont(QFont("Arial", 8, QFont::Bold));

    int row = index.row();
    QString name = model->index(row, 0).data().toString();
    int score = model->index(row, 1).data().toInt();
    // int id = model->index(row, 2).data().toInt();

    painter->drawText(option.rect.adjusted(8, 1, -1, -1), Qt::AlignLeft, index.data().toString());
    painter->drawText(option.rect.adjusted(1, 1, -8, -1), Qt::AlignRight, QString::number(score));
}
