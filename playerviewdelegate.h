#ifndef PLAYERVIEWDELEGATE_H
#define PLAYERVIEWDELEGATE_H

#include <QItemDelegate>
#include <QPainter>

class PlayerViewDelegate : public QItemDelegate
{
public:
    explicit PlayerViewDelegate(QObject *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    QAbstractItemModel *model;
};

#endif
