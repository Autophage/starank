#include "rankingscalculator.h"

#include <iostream>

double probability(int r0, int r1)
{
    return 1.0 / (1 + 1.0 * pow(10, 1.0 * (r1 - r0) / 400));
}

bool RankingsCalculator::calculateRankings(int playerRankings[2], int winner) {
    if (abs(winner) > 1) return false;

    int r0 = playerRankings[0];
    int r1 = playerRankings[1];

    double p0 = probability(r0, r1);

    int delta;
    if (winner == -1) {
        // Tie
        delta = static_cast<int>(K * (0.5 - p0));
        playerRankings[0] += delta;
        playerRankings[1] -= delta;
    } else {
        // A player won
        delta = static_cast<int>(K * (((winner + 1) % 2) - p0));
        playerRankings[0] += delta;
        playerRankings[1] -= delta;
    }

    return true;
}
