#ifndef RANKINGSCALCULATOR_H
#define RANKINGSCALCULATOR_H

#define K 64

class RankingsCalculator
{
public:
    static bool calculateRankings(int* playerRankings, int winner);
};

#endif // RANKINGSCALCULATOR_H
