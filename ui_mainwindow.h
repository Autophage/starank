/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *btnLogo;
    QToolButton *btnPlayers;
    QToolButton *btnGames;
    QToolButton *btnRank;
    QLabel *spacer;
    QStackedWidget *stackedWidget;
    QWidget *homePage;
    QGridLayout *gridLayout_2;
    QLabel *homeLblTitle;
    QTextBrowser *homeBrowserInstructions;
    QWidget *gamesPage;
    QGridLayout *gridLayout_3;
    QLabel *gamesLblScore0;
    QRadioButton *gamesRadioP1;
    QPushButton *gamesBtnUndo;
    QRadioButton *gamesRadioDraw;
    QSpacerItem *verticalSpacer;
    QComboBox *gamesDropdownP0;
    QComboBox *gamesDropdownP1;
    QPushButton *gamesBtnSubmit;
    QRadioButton *gamesRadioP0;
    QLabel *gamesLblScore1;
    QLabel *gamesLblTableTitle;
    QListView *gamesListHistory;
    QWidget *playersPage;
    QGridLayout *gridLayout_4;
    QLabel *playersLblSort;
    QSpacerItem *verticalSpacer_4;
    QComboBox *playersDropdownDisplay;
    QLabel *playersLblAdd;
    QPushButton *playersBtnArchive;
    QPushButton *playersBtnAdd;
    QLabel *playersLblDisplay;
    QLineEdit *playersTxtPlayer;
    QListView *playersList;
    QGridLayout *gridLayout_6;
    QSpacerItem *verticalSpacer_3;
    QLabel *playersLblScoreValue;
    QLabel *playersLblScore;
    QLabel *playersLblName;
    QLabel *playersLblNameValue;
    QLabel *playersLblLossesValue;
    QLabel *playersLblDrawsValue;
    QLabel *playersLblWins;
    QLabel *playersLblWinsValue;
    QLabel *playersLblWinrate;
    QLabel *playersLblLosses;
    QLabel *playersLblDraws;
    QFrame *line;
    QProgressBar *playersWinrate;
    QComboBox *playersDropdownSort;
    QPushButton *playersBtnEdit;
    QSpacerItem *verticalSpacer_2;
    QLabel *label;
    QPushButton *playersBtnDelete;
    QWidget *rankPage;
    QGridLayout *gridLayout_5;
    QLabel *ranksLblNotification;
    QLabel *label_2;
    QTextBrowser *ranksBrowserHTML;
    QPushButton *ranksBtnExportHTML;
    QPushButton *ranksBtnWebBrowserHTML;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(640, 420);
        MainWindow->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"	border: none;\n"
"	background: #880011;\n"
"	color: white;\n"
"	padding: 4px;\n"
"}\n"
"\n"
"QPushButton::disabled {\n"
"	background: gray;\n"
"}\n"
"\n"
"QComboBox {\n"
"	background: transparent;\n"
"	border: none;\n"
"	border-bottom: 2px solid #880011;\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"	border: 0;\n"
"}\n"
"\n"
"QComboBox:hover {\n"
"	background: rgba(108, 108, 108, 55)\n"
"}\n"
"\n"
"QLineEdit {\n"
"	border: none;\n"
"}\n"
"\n"
"QToolButton {\n"
"	background-color: white;\n"
"	border: none;\n"
"	color: transparent;\n"
"	padding-top: 6%;\n"
"}\n"
"\n"
"QToolButton:hover {\n"
"	color: gray;\n"
"}\n"
"\n"
"QToolButton:checked {\n"
"	background: transparent;\n"
"	color: black;\n"
"}\n"
"\n"
"\n"
"QRadioButton:indicator {\n"
"	border-radius: 6px;\n"
"	border: 1px solid black;\n"
"	background: white;\n"
"	width: 10px;\n"
"	height: 10px;\n"
"}\n"
"\n"
"QRadioButton:indicator:checked {\n"
"	color: #880011;\n"
"	background: qradialgradient(cx:0, cy:0, radius: 1, fx:1, fy:1, stop:0.25 #8800"
                        "11, stop:0.3 white);\n"
"	border-radius: 6px;\n"
"	border: 2px solid #880011;\n"
"}\n"
"\n"
"QListView {\n"
"	border: none;\n"
"}\n"
"\n"
"QTableView {\n"
"	border: none;\n"
"}\n"
"\n"
"QTextBrowser {\n"
"	border: none;\n"
"	background: transparent;\n"
"}"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        btnLogo = new QPushButton(centralWidget);
        btnLogo->setObjectName(QString::fromUtf8("btnLogo"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(btnLogo->sizePolicy().hasHeightForWidth());
        btnLogo->setSizePolicy(sizePolicy);
        btnLogo->setMaximumSize(QSize(100, 100));
        btnLogo->setBaseSize(QSize(100, 100));
        btnLogo->setStyleSheet(QString::fromUtf8("margin: 0;\n"
"padding: 0;"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("img/STARank.svg"), QSize(), QIcon::Normal, QIcon::Off);
        btnLogo->setIcon(icon);
        btnLogo->setIconSize(QSize(100, 100));
        btnLogo->setFlat(true);

        verticalLayout->addWidget(btnLogo);

        btnPlayers = new QToolButton(centralWidget);
        btnPlayers->setObjectName(QString::fromUtf8("btnPlayers"));
        sizePolicy.setHeightForWidth(btnPlayers->sizePolicy().hasHeightForWidth());
        btnPlayers->setSizePolicy(sizePolicy);
        btnPlayers->setMaximumSize(QSize(100, 100));
        btnPlayers->setBaseSize(QSize(100, 100));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        btnPlayers->setFont(font);
        btnPlayers->setStyleSheet(QString::fromUtf8(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("img/account-group.svg"), QSize(), QIcon::Normal, QIcon::Off);
        btnPlayers->setIcon(icon1);
        btnPlayers->setIconSize(QSize(64, 64));
        btnPlayers->setCheckable(true);
        btnPlayers->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        verticalLayout->addWidget(btnPlayers);

        btnGames = new QToolButton(centralWidget);
        btnGames->setObjectName(QString::fromUtf8("btnGames"));
        sizePolicy.setHeightForWidth(btnGames->sizePolicy().hasHeightForWidth());
        btnGames->setSizePolicy(sizePolicy);
        btnGames->setMaximumSize(QSize(100, 100));
        btnGames->setBaseSize(QSize(100, 100));
        btnGames->setFont(font);
        btnGames->setStyleSheet(QString::fromUtf8(""));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("img/tournament.svg"), QSize(), QIcon::Normal, QIcon::Off);
        btnGames->setIcon(icon2);
        btnGames->setIconSize(QSize(64, 64));
        btnGames->setCheckable(true);
        btnGames->setChecked(false);
        btnGames->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        verticalLayout->addWidget(btnGames);

        btnRank = new QToolButton(centralWidget);
        btnRank->setObjectName(QString::fromUtf8("btnRank"));
        sizePolicy.setHeightForWidth(btnRank->sizePolicy().hasHeightForWidth());
        btnRank->setSizePolicy(sizePolicy);
        btnRank->setMaximumSize(QSize(100, 100));
        btnRank->setFont(font);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("img/trophy-award.svg"), QSize(), QIcon::Normal, QIcon::Off);
        btnRank->setIcon(icon3);
        btnRank->setIconSize(QSize(64, 64));
        btnRank->setCheckable(true);
        btnRank->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        verticalLayout->addWidget(btnRank);

        spacer = new QLabel(centralWidget);
        spacer->setObjectName(QString::fromUtf8("spacer"));
        spacer->setStyleSheet(QString::fromUtf8("background: white;"));

        verticalLayout->addWidget(spacer);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy1);
        stackedWidget->setMinimumSize(QSize(400, 400));
        stackedWidget->setBaseSize(QSize(800, 800));
        homePage = new QWidget();
        homePage->setObjectName(QString::fromUtf8("homePage"));
        gridLayout_2 = new QGridLayout(homePage);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        homeLblTitle = new QLabel(homePage);
        homeLblTitle->setObjectName(QString::fromUtf8("homeLblTitle"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(homeLblTitle->sizePolicy().hasHeightForWidth());
        homeLblTitle->setSizePolicy(sizePolicy2);
        QFont font1;
        font1.setPointSize(36);
        font1.setBold(true);
        font1.setWeight(75);
        homeLblTitle->setFont(font1);
        homeLblTitle->setContextMenuPolicy(Qt::NoContextMenu);
        homeLblTitle->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_2->addWidget(homeLblTitle, 0, 1, 1, 1);

        homeBrowserInstructions = new QTextBrowser(homePage);
        homeBrowserInstructions->setObjectName(QString::fromUtf8("homeBrowserInstructions"));
        homeBrowserInstructions->setStyleSheet(QString::fromUtf8(""));

        gridLayout_2->addWidget(homeBrowserInstructions, 1, 1, 1, 1);

        stackedWidget->addWidget(homePage);
        gamesPage = new QWidget();
        gamesPage->setObjectName(QString::fromUtf8("gamesPage"));
        gridLayout_3 = new QGridLayout(gamesPage);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gamesLblScore0 = new QLabel(gamesPage);
        gamesLblScore0->setObjectName(QString::fromUtf8("gamesLblScore0"));

        gridLayout_3->addWidget(gamesLblScore0, 0, 0, 1, 1);

        gamesRadioP1 = new QRadioButton(gamesPage);
        gamesRadioP1->setObjectName(QString::fromUtf8("gamesRadioP1"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(2);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(gamesRadioP1->sizePolicy().hasHeightForWidth());
        gamesRadioP1->setSizePolicy(sizePolicy3);
        QFont font2;
        font2.setPointSize(10);
        gamesRadioP1->setFont(font2);

        gridLayout_3->addWidget(gamesRadioP1, 3, 1, 1, 1);

        gamesBtnUndo = new QPushButton(gamesPage);
        gamesBtnUndo->setObjectName(QString::fromUtf8("gamesBtnUndo"));
        gamesBtnUndo->setMinimumSize(QSize(100, 0));
        gamesBtnUndo->setMaximumSize(QSize(100, 16777215));

        gridLayout_3->addWidget(gamesBtnUndo, 6, 2, 1, 1);

        gamesRadioDraw = new QRadioButton(gamesPage);
        gamesRadioDraw->setObjectName(QString::fromUtf8("gamesRadioDraw"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(gamesRadioDraw->sizePolicy().hasHeightForWidth());
        gamesRadioDraw->setSizePolicy(sizePolicy4);
        gamesRadioDraw->setFont(font2);
        gamesRadioDraw->setStyleSheet(QString::fromUtf8("margin-left: 16px;"));
        gamesRadioDraw->setCheckable(true);
        gamesRadioDraw->setChecked(false);

        gridLayout_3->addWidget(gamesRadioDraw, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 5, 0, 1, 3);

        gamesDropdownP0 = new QComboBox(gamesPage);
        gamesDropdownP0->setObjectName(QString::fromUtf8("gamesDropdownP0"));

        gridLayout_3->addWidget(gamesDropdownP0, 1, 0, 1, 1);

        gamesDropdownP1 = new QComboBox(gamesPage);
        gamesDropdownP1->setObjectName(QString::fromUtf8("gamesDropdownP1"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(3);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(gamesDropdownP1->sizePolicy().hasHeightForWidth());
        gamesDropdownP1->setSizePolicy(sizePolicy5);

        gridLayout_3->addWidget(gamesDropdownP1, 3, 0, 1, 1);

        gamesBtnSubmit = new QPushButton(gamesPage);
        gamesBtnSubmit->setObjectName(QString::fromUtf8("gamesBtnSubmit"));
        gamesBtnSubmit->setEnabled(false);
        gamesBtnSubmit->setMinimumSize(QSize(0, 24));
        QFont font3;
        font3.setBold(true);
        font3.setWeight(75);
        gamesBtnSubmit->setFont(font3);
        gamesBtnSubmit->setStyleSheet(QString::fromUtf8(""));

        gridLayout_3->addWidget(gamesBtnSubmit, 1, 2, 3, 1);

        gamesRadioP0 = new QRadioButton(gamesPage);
        gamesRadioP0->setObjectName(QString::fromUtf8("gamesRadioP0"));
        sizePolicy4.setHeightForWidth(gamesRadioP0->sizePolicy().hasHeightForWidth());
        gamesRadioP0->setSizePolicy(sizePolicy4);
        gamesRadioP0->setFont(font2);
        gamesRadioP0->setChecked(false);

        gridLayout_3->addWidget(gamesRadioP0, 1, 1, 1, 1);

        gamesLblScore1 = new QLabel(gamesPage);
        gamesLblScore1->setObjectName(QString::fromUtf8("gamesLblScore1"));

        gridLayout_3->addWidget(gamesLblScore1, 4, 0, 1, 1);

        gamesLblTableTitle = new QLabel(gamesPage);
        gamesLblTableTitle->setObjectName(QString::fromUtf8("gamesLblTableTitle"));
        QFont font4;
        font4.setBold(false);
        font4.setWeight(50);
        gamesLblTableTitle->setFont(font4);

        gridLayout_3->addWidget(gamesLblTableTitle, 6, 0, 1, 1);

        gamesListHistory = new QListView(gamesPage);
        gamesListHistory->setObjectName(QString::fromUtf8("gamesListHistory"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(1);
        sizePolicy6.setHeightForWidth(gamesListHistory->sizePolicy().hasHeightForWidth());
        gamesListHistory->setSizePolicy(sizePolicy6);
        gamesListHistory->setStyleSheet(QString::fromUtf8(""));

        gridLayout_3->addWidget(gamesListHistory, 7, 0, 1, 3);

        stackedWidget->addWidget(gamesPage);
        playersPage = new QWidget();
        playersPage->setObjectName(QString::fromUtf8("playersPage"));
        QSizePolicy sizePolicy7(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(1);
        sizePolicy7.setVerticalStretch(1);
        sizePolicy7.setHeightForWidth(playersPage->sizePolicy().hasHeightForWidth());
        playersPage->setSizePolicy(sizePolicy7);
        playersPage->setBaseSize(QSize(400, 400));
        playersPage->setStyleSheet(QString::fromUtf8(""));
        gridLayout_4 = new QGridLayout(playersPage);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        playersLblSort = new QLabel(playersPage);
        playersLblSort->setObjectName(QString::fromUtf8("playersLblSort"));

        gridLayout_4->addWidget(playersLblSort, 3, 3, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_4, 2, 0, 1, 6);

        playersDropdownDisplay = new QComboBox(playersPage);
        playersDropdownDisplay->addItem(QString());
        playersDropdownDisplay->addItem(QString());
        playersDropdownDisplay->setObjectName(QString::fromUtf8("playersDropdownDisplay"));
        QSizePolicy sizePolicy8(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy8.setHorizontalStretch(1);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(playersDropdownDisplay->sizePolicy().hasHeightForWidth());
        playersDropdownDisplay->setSizePolicy(sizePolicy8);

        gridLayout_4->addWidget(playersDropdownDisplay, 3, 1, 1, 1);

        playersLblAdd = new QLabel(playersPage);
        playersLblAdd->setObjectName(QString::fromUtf8("playersLblAdd"));
        sizePolicy2.setHeightForWidth(playersLblAdd->sizePolicy().hasHeightForWidth());
        playersLblAdd->setSizePolicy(sizePolicy2);

        gridLayout_4->addWidget(playersLblAdd, 0, 0, 1, 5);

        playersBtnArchive = new QPushButton(playersPage);
        playersBtnArchive->setObjectName(QString::fromUtf8("playersBtnArchive"));

        gridLayout_4->addWidget(playersBtnArchive, 9, 5, 1, 1);

        playersBtnAdd = new QPushButton(playersPage);
        playersBtnAdd->setObjectName(QString::fromUtf8("playersBtnAdd"));

        gridLayout_4->addWidget(playersBtnAdd, 1, 5, 1, 1);

        playersLblDisplay = new QLabel(playersPage);
        playersLblDisplay->setObjectName(QString::fromUtf8("playersLblDisplay"));
        sizePolicy1.setHeightForWidth(playersLblDisplay->sizePolicy().hasHeightForWidth());
        playersLblDisplay->setSizePolicy(sizePolicy1);

        gridLayout_4->addWidget(playersLblDisplay, 3, 0, 1, 1);

        playersTxtPlayer = new QLineEdit(playersPage);
        playersTxtPlayer->setObjectName(QString::fromUtf8("playersTxtPlayer"));

        gridLayout_4->addWidget(playersTxtPlayer, 1, 0, 1, 5);

        playersList = new QListView(playersPage);
        playersList->setObjectName(QString::fromUtf8("playersList"));
        sizePolicy6.setHeightForWidth(playersList->sizePolicy().hasHeightForWidth());
        playersList->setSizePolicy(sizePolicy6);

        gridLayout_4->addWidget(playersList, 4, 0, 7, 5);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(10);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_3, 3, 0, 1, 2);

        playersLblScoreValue = new QLabel(playersPage);
        playersLblScoreValue->setObjectName(QString::fromUtf8("playersLblScoreValue"));
        playersLblScoreValue->setMinimumSize(QSize(120, 0));

        gridLayout_6->addWidget(playersLblScoreValue, 2, 1, 1, 1);

        playersLblScore = new QLabel(playersPage);
        playersLblScore->setObjectName(QString::fromUtf8("playersLblScore"));

        gridLayout_6->addWidget(playersLblScore, 2, 0, 1, 1);

        playersLblName = new QLabel(playersPage);
        playersLblName->setObjectName(QString::fromUtf8("playersLblName"));

        gridLayout_6->addWidget(playersLblName, 1, 0, 1, 1);

        playersLblNameValue = new QLabel(playersPage);
        playersLblNameValue->setObjectName(QString::fromUtf8("playersLblNameValue"));
        playersLblNameValue->setMinimumSize(QSize(120, 0));

        gridLayout_6->addWidget(playersLblNameValue, 1, 1, 1, 1);

        playersLblLossesValue = new QLabel(playersPage);
        playersLblLossesValue->setObjectName(QString::fromUtf8("playersLblLossesValue"));
        playersLblLossesValue->setMinimumSize(QSize(120, 0));

        gridLayout_6->addWidget(playersLblLossesValue, 6, 1, 1, 1);

        playersLblDrawsValue = new QLabel(playersPage);
        playersLblDrawsValue->setObjectName(QString::fromUtf8("playersLblDrawsValue"));
        playersLblDrawsValue->setMinimumSize(QSize(120, 0));

        gridLayout_6->addWidget(playersLblDrawsValue, 7, 1, 1, 1);

        playersLblWins = new QLabel(playersPage);
        playersLblWins->setObjectName(QString::fromUtf8("playersLblWins"));

        gridLayout_6->addWidget(playersLblWins, 4, 0, 1, 1);

        playersLblWinsValue = new QLabel(playersPage);
        playersLblWinsValue->setObjectName(QString::fromUtf8("playersLblWinsValue"));
        playersLblWinsValue->setMinimumSize(QSize(120, 0));

        gridLayout_6->addWidget(playersLblWinsValue, 4, 1, 1, 1);

        playersLblWinrate = new QLabel(playersPage);
        playersLblWinrate->setObjectName(QString::fromUtf8("playersLblWinrate"));

        gridLayout_6->addWidget(playersLblWinrate, 8, 0, 1, 1);

        playersLblLosses = new QLabel(playersPage);
        playersLblLosses->setObjectName(QString::fromUtf8("playersLblLosses"));

        gridLayout_6->addWidget(playersLblLosses, 6, 0, 1, 1);

        playersLblDraws = new QLabel(playersPage);
        playersLblDraws->setObjectName(QString::fromUtf8("playersLblDraws"));

        gridLayout_6->addWidget(playersLblDraws, 7, 0, 1, 1);

        line = new QFrame(playersPage);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_6->addWidget(line, 0, 0, 1, 2);

        playersWinrate = new QProgressBar(playersPage);
        playersWinrate->setObjectName(QString::fromUtf8("playersWinrate"));
        playersWinrate->setStyleSheet(QString::fromUtf8("QProgressBar::chunk {\n"
"background: #880011;\n"
"text-align: right;\n"
"}\n"
"\n"
"QProgressBar:horizontal {\n"
"text-align: right;\n"
"margin-right: 12ex;\n"
"}"));
        playersWinrate->setValue(0);

        gridLayout_6->addWidget(playersWinrate, 8, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_6, 4, 5, 3, 1);

        playersDropdownSort = new QComboBox(playersPage);
        playersDropdownSort->addItem(QString());
        playersDropdownSort->addItem(QString());
        playersDropdownSort->setObjectName(QString::fromUtf8("playersDropdownSort"));
        sizePolicy8.setHeightForWidth(playersDropdownSort->sizePolicy().hasHeightForWidth());
        playersDropdownSort->setSizePolicy(sizePolicy8);

        gridLayout_4->addWidget(playersDropdownSort, 3, 4, 1, 1);

        playersBtnEdit = new QPushButton(playersPage);
        playersBtnEdit->setObjectName(QString::fromUtf8("playersBtnEdit"));

        gridLayout_4->addWidget(playersBtnEdit, 8, 5, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 7, 5, 1, 1);

        label = new QLabel(playersPage);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(true);
        font5.setUnderline(false);
        font5.setWeight(75);
        label->setFont(font5);
        label->setStyleSheet(QString::fromUtf8("border-bottom: 2px solid black;"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label, 3, 5, 1, 1);

        playersBtnDelete = new QPushButton(playersPage);
        playersBtnDelete->setObjectName(QString::fromUtf8("playersBtnDelete"));
        playersBtnDelete->setEnabled(true);
        playersBtnDelete->setStyleSheet(QString::fromUtf8(""));

        gridLayout_4->addWidget(playersBtnDelete, 10, 5, 1, 1);

        stackedWidget->addWidget(playersPage);
        rankPage = new QWidget();
        rankPage->setObjectName(QString::fromUtf8("rankPage"));
        rankPage->setStyleSheet(QString::fromUtf8(""));
        gridLayout_5 = new QGridLayout(rankPage);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        ranksLblNotification = new QLabel(rankPage);
        ranksLblNotification->setObjectName(QString::fromUtf8("ranksLblNotification"));
        ranksLblNotification->setEnabled(true);
        QFont font6;
        font6.setPointSize(10);
        font6.setBold(true);
        font6.setWeight(75);
        ranksLblNotification->setFont(font6);
        ranksLblNotification->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(ranksLblNotification, 1, 1, 1, 1);

        label_2 = new QLabel(rankPage);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_5->addWidget(label_2, 1, 0, 1, 1);

        ranksBrowserHTML = new QTextBrowser(rankPage);
        ranksBrowserHTML->setObjectName(QString::fromUtf8("ranksBrowserHTML"));
        ranksBrowserHTML->setStyleSheet(QString::fromUtf8("background: white;"));

        gridLayout_5->addWidget(ranksBrowserHTML, 2, 0, 1, 2);

        ranksBtnExportHTML = new QPushButton(rankPage);
        ranksBtnExportHTML->setObjectName(QString::fromUtf8("ranksBtnExportHTML"));
        QSizePolicy sizePolicy9(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(ranksBtnExportHTML->sizePolicy().hasHeightForWidth());
        ranksBtnExportHTML->setSizePolicy(sizePolicy9);

        gridLayout_5->addWidget(ranksBtnExportHTML, 3, 0, 1, 2);

        ranksBtnWebBrowserHTML = new QPushButton(rankPage);
        ranksBtnWebBrowserHTML->setObjectName(QString::fromUtf8("ranksBtnWebBrowserHTML"));

        gridLayout_5->addWidget(ranksBtnWebBrowserHTML, 5, 0, 1, 2);

        stackedWidget->addWidget(rankPage);

        gridLayout->addWidget(stackedWidget, 0, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
#if QT_CONFIG(shortcut)
        gamesLblScore0->setBuddy(gamesLblScore0);
        gamesLblScore1->setBuddy(gamesLblScore1);
#endif // QT_CONFIG(shortcut)

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About", nullptr));
        btnLogo->setText(QString());
        btnPlayers->setText(QCoreApplication::translate("MainWindow", "Players", nullptr));
        btnGames->setText(QCoreApplication::translate("MainWindow", "Games", nullptr));
        btnRank->setText(QCoreApplication::translate("MainWindow", "Ranks", nullptr));
        spacer->setText(QString());
        homeLblTitle->setText(QCoreApplication::translate("MainWindow", "STARank", nullptr));
        homeBrowserInstructions->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">STARank</span> is a program that uses the ELO ranking algorithm to judge the relative skill levels between a group of players.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\""
                        " style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">To begin, enter the <span style=\" font-weight:600;\">Players</span> tab and create some new players in your group. From here, you may also view some statistics for each player. Removing players is permitted, however, only if the player has been newly added and has no games in their record. Once a player has played a game with another player, they may no longer be removed, to preserve the accuracy of other players' rankings.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">You may then go to the <span style=\" font-weight:600;\">Games</span> tab to enter the results of any games. Select the two opposing players from the dro"
                        "pdowns, and check off the result of the game. The history panel will display all games entered in this session. If a mistake is made, games may be struck from this history and changes in rank reversed. However, all changes must be done sequentially, starting with the most recent game. Additionally, games may only be removed in the session they were recorded, so once you close the program, all changes that have been made are now final.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Finally, the <span style=\" font-weight:600;\">Ranks</span> tab allows you to export an HTML table of the current player rankings, which will appear within the &quot;Outputs&quot; folder in the <span style=\" font-weight:600;\">STARank</span> program directory.<"
                        "/p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">To  return to this page at any time, you may click the logo in the top left.</p></body></html>", nullptr));
        gamesLblScore0->setText(QCoreApplication::translate("MainWindow", "Score:", nullptr));
        gamesRadioP1->setText(QCoreApplication::translate("MainWindow", "Winner", nullptr));
        gamesBtnUndo->setText(QCoreApplication::translate("MainWindow", "Undo", nullptr));
        gamesRadioDraw->setText(QCoreApplication::translate("MainWindow", "Draw", nullptr));
        gamesBtnSubmit->setText(QCoreApplication::translate("MainWindow", "Submit", nullptr));
        gamesRadioP0->setText(QCoreApplication::translate("MainWindow", "Winner", nullptr));
        gamesLblScore1->setText(QCoreApplication::translate("MainWindow", "Score:", nullptr));
        gamesLblTableTitle->setText(QCoreApplication::translate("MainWindow", "History:", nullptr));
        playersLblSort->setText(QCoreApplication::translate("MainWindow", "Sort By:", nullptr));
        playersDropdownDisplay->setItemText(0, QCoreApplication::translate("MainWindow", "Active", nullptr));
        playersDropdownDisplay->setItemText(1, QCoreApplication::translate("MainWindow", "Archived", nullptr));

        playersLblAdd->setText(QCoreApplication::translate("MainWindow", "Add a Player:", nullptr));
        playersBtnArchive->setText(QCoreApplication::translate("MainWindow", "Archive Player", nullptr));
        playersBtnAdd->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
        playersLblDisplay->setText(QCoreApplication::translate("MainWindow", "Display:", nullptr));
        playersLblScoreValue->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        playersLblScore->setText(QCoreApplication::translate("MainWindow", "Score:", nullptr));
        playersLblName->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        playersLblNameValue->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        playersLblLossesValue->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        playersLblDrawsValue->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        playersLblWins->setText(QCoreApplication::translate("MainWindow", "Wins:", nullptr));
        playersLblWinsValue->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        playersLblWinrate->setText(QCoreApplication::translate("MainWindow", "Win %:", nullptr));
        playersLblLosses->setText(QCoreApplication::translate("MainWindow", "Losses:", nullptr));
        playersLblDraws->setText(QCoreApplication::translate("MainWindow", "Draws:", nullptr));
        playersDropdownSort->setItemText(0, QCoreApplication::translate("MainWindow", "Name", nullptr));
        playersDropdownSort->setItemText(1, QCoreApplication::translate("MainWindow", "Score", nullptr));

        playersBtnEdit->setText(QCoreApplication::translate("MainWindow", "Edit Name", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Player Info", nullptr));
        playersBtnDelete->setText(QCoreApplication::translate("MainWindow", "Delete Player", nullptr));
        ranksLblNotification->setText(QString());
        label_2->setText(QCoreApplication::translate("MainWindow", "HTML Table Code:", nullptr));
        ranksBtnExportHTML->setText(QCoreApplication::translate("MainWindow", "Generate HTML", nullptr));
        ranksBtnWebBrowserHTML->setText(QCoreApplication::translate("MainWindow", "Open HTML In Web Browser", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
